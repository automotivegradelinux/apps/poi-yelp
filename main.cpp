#include <QApplication>
#include <iostream>
#include "MainApp.h"
#include <getopt.h>

#include <libhomescreen.hpp>
#include <navigation.h>

#define DEFAULT_CREDENTIALS_FILE "/etc/poikey"

using namespace std;

static QString graphic_role;
static MainApp *mainapp;
static LibHomeScreen* hs;

// Callback to drive raising navigation app
static void NavWindowRaiseHandler(void)
{
    if (hs) {
        hs->showWindow("navigation", nullptr);
    }
}

static void ShowWindowHandler(json_object *object)
{

}

int main(int argc, char *argv[], char *env[])
{
    int opt;
    QApplication a(argc, argv);
    QString credentialsFile(DEFAULT_CREDENTIALS_FILE);

    hs = new LibHomeScreen();
    graphic_role = QString("poi");
    a.setDesktopFileName(graphic_role);

    QString pt = QString(argv[1]);
    int port = pt.toInt();
    QString secret = QString(argv[2]);
    std::string token = secret.toStdString();

    QUrl bindingAddress;
    bindingAddress.setScheme(QStringLiteral("ws"));
    bindingAddress.setHost(QStringLiteral("localhost"));
    bindingAddress.setPort(port);
    bindingAddress.setPath(QStringLiteral("/api"));
    QUrlQuery query;
    query.addQueryItem(QStringLiteral("token"), secret);
    bindingAddress.setQuery(query);

    mainapp = new MainApp(new Navigation(bindingAddress));

    // force setting
    mainapp->setInfoScreen(true);
    mainapp->setKeyboard(true);

    // hook up callback to start/raise navigation app
    mainapp->setNavWindowRaiseCallback(NavWindowRaiseHandler);

    hs->init(port, token.c_str());
    hs->set_event_handler(LibHomeScreen::Event_ShowWindow, ShowWindowHandler);

    /* then, authenticate connexion to POI service: */
    if (mainapp->AuthenticatePOI(credentialsFile) < 0)
    {
        cerr << "Error: POI server authentication failed" << endl;
        return -1;
    }

    cerr << "authentication succes !" << endl;

    /* now, let's start monitor user inut (register callbacks): */
    if (mainapp->StartMonitoringUserInput() < 0)
        return -1;

    /* main loop: */
    return a.exec();
}
